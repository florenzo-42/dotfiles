" ...«
set nocompatible
syntax on
filetype plugin indent on
let mapleader = " "
nnoremap <Leader> <Nop>
"»
" Plugins«
if (!has('nvim') && empty(glob('~/.vim/autoload/plug.vim')))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
if (has('nvim') && empty(glob('~/.local/share/nvim/site/autoload/plug.vim')))
	silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-fugitive'
" Plug 'andymass/vim-matchup' " This is conflicting with something... but what ?
Plug 'wellle/targets.vim'
Plug 'tommcdo/vim-exchange'
Plug 'tpope/vim-surround'
Plug 'justinmk/vim-sneak'
map f <Plug>Sneak_f
map F <Plug>Sneak_F
map t <Plug>Sneak_t
map T <Plug>Sneak_T
Plug 'easymotion/vim-easymotion'
map <Enter> <Plug>(easymotion-prefix)
Plug 'tomtom/tcomment_vim'
let g:windowswap_map_keys = 0
Plug 'wesQ3/vim-windowswap'
nnoremap <silent> <C-W>y :call WindowSwap#EasyWindowSwap()<CR>
Plug 'AndrewRadev/splitjoin.vim'
Plug 'guns/xterm-color-table.vim'
Plug 'thiagoalessio/rainbow_levels.vim'
Plug 'terryma/vim-smooth-scroll'
" 3 params: how many lines to scroll, how long lasts a frame in ms, how many lines per frame
" if i press <C-U> on a 70 lines high screen, it will take 70 / 2 (height) / 1 * 2 = 70 ms
nnoremap <silent> <C-U> :call smooth_scroll#up(&scroll, 2, 1)<CR>
nnoremap <silent> <C-D> :call smooth_scroll#down(&scroll, 2, 1)<CR>
nnoremap <silent> <C-B> :call smooth_scroll#up(&scroll*2, 1, 1)<CR>
nnoremap <silent> <C-F> :call smooth_scroll#down(&scroll*2, 1, 1)<CR>
Plug 'sheerun/vim-polyglot'
let g:polyglot_disabled = ['vue', 'coffee', 'coffee-script',]
Plug 'posva/vim-vue'
Plug 'mbbill/undotree'
nnoremap <silent> <Leader>u :UndotreeShow<CR>
Plug 'tpope/vim-abolish'
Plug 'lfv89/vim-interestingwords'
Plug 'Claperius/random-vim'
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'
" let g:UltiSnipsExpandTrigger="<C-Y>"
" let g:UltiSnipsJumpForwardTrigger="<C-T>"
" let g:UltiSnipsJumpBackwardTrigger="<C-J>"
" Plug 'junegunn/fzf'
" Plug 'Shougo/denite.nvim'
" nnoremap <silent> <Leader>df :Denite file<CR>
if (v:version >= 800 || has('nvim'))
	Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }
endif
if !has('nvim')
	Plug 'roxma/nvim-yarp'
	Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gt <Plug>(coc-type-definition)
nmap <silent> gy <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>rn <Plug>(coc-rename)
nmap <leader>l <Plug>(coc-codelens-action)

Plug 'docunext/closetag.vim', { 'for' : 'html,xhtml,css,php,jst,jsx,js,vue,javascript.jsx' }
Plug 'othree/jsdoc-syntax.vim'
Plug 'machakann/vim-highlightedyank'
call plug#end()
"»
" Options«
if (has('mouse'))
	set mouse=a
endif
if has('reltime')
	set incsearch
endif
set autoindent
set autoread
set cursorline
set backspace=indent,eol,start
set breakindent							" Keep the indent of the beggining of the broken line
if (has('nvim'))
	set inccommand=nosplit
endif
set fillchars=fold:\ ,vert:\│
set foldlevelstart=99					" Open all folds by default
set foldmethod=syntax
set foldminlines=2						" 1 line folds by default, really?
set hidden
set history=5000
set hlsearch
set ignorecase
set iskeyword=a-z,A-Z,48-57,_,@,$
set laststatus=2						" Always show the status line
set lazyredraw
set linebreak							" Visually break a line at a whitespace character
set noequalalways
set noexpandtab
set nrformats-=octal					" Disable octal numbers
set number
set relativenumber
set scrolloff=5
set shiftwidth=4
set showcmd
set showmatch							" Jump to matching parenthesis when typing
set showmode
set smartcase
set splitbelow
set splitright
set tabstop=4
set winminheight=0
set winminwidth=0
let &t_ZH="\e[3m"						" Set italic escape codes for old terminals
let &t_ZR="\e[23m"						" Hard to believe i had to do that
"»
" Directories«
set undodir=~/.vim/undo//
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
if !isdirectory(expand(&undodir))
	call mkdir(expand(&undodir), "p")
endif
if !isdirectory(expand(&backupdir))
	call mkdir(expand(&backupdir), "p")
endif
if !isdirectory(expand(&directory))
	call mkdir(expand(&directory), "p")
endif
set backup
if has('persistent_undo')
	set undofile
endif
"»
" Wildmenu«
set path+=**
set wildmenu						" Display completion matches in a status line
set wildignorecase
set wildmode=longest,list,full
" File completion similar to bash
set wildignore+=*~,*.swp,*.tmp
set wildignore+=*.o,*.out,*.a,*.dSYM/
set wildignore+=*.png,*.jpg
"»
" Colors«
colorscheme default
set background=dark
hi Comment cterm=italic ctermfg=248
hi Visual ctermbg=236
hi Statusline cterm=bold ctermbg=24
hi clear Folded
hi Folded ctermfg=248
hi Function ctermfg=178
hi clear MatchParen
hi MatchParen cterm=underline
hi Constant ctermfg=46
hi Statement cterm=bold ctermfg=10
hi Number ctermfg=200
hi String ctermfg=229
hi clear VertSplit
hi VertSplit ctermfg=111
hi clear StatusLineNC
hi StatusLineNC ctermbg=238
hi CursorLineNr cterm=bold,underline ctermfg=111
hi clear TabLine
hi clear TabLineFill
hi TabLineSel ctermbg=239 cterm=bold
hi CursorLine cterm=bold ctermbg=233
hi clear pythonSpaceError
"»
" Statusline«
set statusline=%f					" Path to the file
set statusline+=\ -\ 
set statusline+=%y					" Filetype of the file
" set statusline+=\ %{coc#status()}
" set statusline+=\ %{FugitiveStatusline()}
set statusline+=%=					" Switch to the right
set statusline+=%m					" Modified flag
set statusline+=\ %v				"Column number
"»
" Global Mappings«
inoremap <C-U> <Esc>gUiWEa
nnoremap <expr> j v:count ? 'j' : 'gj'
nnoremap <expr> k v:count ? 'k' : 'gk'
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <Leader><C-L> <C-L>
nnoremap K zk
nnoremap J zj
nnoremap L J
nnoremap Q zA
nnoremap + <C-W>+
nnoremap - <C-W>-
nnoremap <Left> <C-W><
nnoremap <Right> <C-W>>
" These are some really "bad-practice" mappings haha
nnoremap <S-Left> 10<C-W><
nnoremap <S-Right> 10<C-W>>
nnoremap <Up> gT
nnoremap <Down> gt
" nnoremap <A-k> :m-2<CR>
" nnoremap <A-j> :m+1<CR>
nnoremap <Leader>f <C-W>vgf
nnoremap <Leader>] <C-W>v<C-]>
nnoremap <Leader>w :update<CR>
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>tv :tabe $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
nnoremap <Leader>an :next<CR>
nnoremap <Leader>ap :prev<CR>
nnoremap <Leader>af :first<CR>
nnoremap <Leader>al :last<CR>
nnoremap <Leader>eo :copen<CR>
nnoremap <Leader>ec :cclose<CR>
nnoremap <Leader>en :cnext<CR>
nnoremap <Leader>ep :cprev<CR>
nnoremap <Leader>ef :cfirst<CR>
nnoremap <Leader>el :clast<CR>
nnoremap <Leader>lo :lopen<CR>
nnoremap <Leader>lc :lclose<CR>
nnoremap <Leader>ln :lnext<CR>
nnoremap <Leader>lp :lprev<CR>
nnoremap <Leader>lf :lfirst<CR>
nnoremap <Leader>ll :llast<CR>
nnoremap <Leader>sp :set paste<CR>
nnoremap <Leader>snp :set nopaste<CR>
nnoremap <Leader>vt :vs\|te<CR>i
nnoremap <Leader>st :sp\|te<CR>i
nnoremap <Leader>bt :b te<Tab><CR>
nnoremap <Leader>= mmgg=G`m
" fullscreen the current window... some people use plugins just for this lmfao
nnoremap <C-W>f <C-W>_<C-W>\|
augroup globalmappings
	autocmd!
	autocmd bufreadpost *
				\ if line("'\"") >= 1 && line("'\"") <= line("$") |
				\   exe "normal! g`\"" |
				\ endif
augroup end
if !exists(":DiffOrig")
	command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
				\ | wincmd p | diffthis
endif
"»
" Emacs-style editing«
cnoremap <C-A>      <Home>
inoremap <C-B>		<Left>
cnoremap <C-B>		<Left>
inoremap <C-E>		<End>
cnoremap <C-O>a		<C-A>
cnoremap <C-O>A		<C-A>
"»
" Neovim«
if (has('nvim'))
	tnoremap <Esc> <C-\><C-N>
	augroup nvim
		autocmd!
		autocmd TermOpen * setlocal nocursorline
		autocmd TermOpen * setlocal statusline=%{b:term_title}
	augroup end
endif
"»
" Programming stuff«
augroup programming
	autocmd!
	autocmd Filetype * nnoremap <buffer> <Leader>rm :call RemoveExtraWhiteSpaces()<CR>
	autocmd FileType * nnoremap <buffer> <Leader>C :normal gccj<C-J>
	autocmd FileType * nnoremap <buffer> <Leader>c :normal gccj<C-J>
	autocmd FileType * noremap <buffer> <Leader>; :normal! mmA;<C-[>`m
	autocmd FileType html,jst,json,javascript setlocal ts=2 sw=2 et
	autocmd FileType lisp inoreabbrev <buffer> nil NIL
	autocmd FileType php inoreabbrev <buffer> eol PHP_EOL
	autocmd FileType tex nnoremap <buffer> <Leader>mt :!pdflatex %<CR>
	autocmd FileType javascript inoreabbrev <buffer> fn function
	autocmd FileType javascript inoreabbrev <buffer> c const
	autocmd FileType javascript inoreabbrev <buffer> l let
	autocmd FileType javascript inoreabbrev <buffer> un undefined
	autocmd FileType javascript inoreabbrev <buffer> isun === undefined
	autocmd FileType javascript inoreabbrev <buffer> nu null
	autocmd FileType javascript inoreabbrev <buffer> nan NaN
	autocmd FileType javascript inoreabbrev <buffer> math Math
	autocmd FileType javascript inoreabbrev <buffer> num Number
	autocmd FileType javascript inoreabbrev <buffer> obj Object
	autocmd FileType javascript inoreabbrev <buffer> er error
	autocmd FileType javascript inoreabbrev <buffer> rq require
	autocmd FileType javascript inoreabbrev <buffer> clg console.log
	autocmd FileType javascript inoreabbrev <buffer> cer console.error
	autocmd FileType javascript inoreabbrev <buffer> ast assert
	autocmd FileType javascript inoreabbrev <buffer> eq equal
	autocmd FileType javascript inoreabbrev <buffer> rs result
	autocmd FileType javascript inoreabbrev <buffer> msg message
	autocmd FileType javascript inoreabbrev <buffer> aw await
	autocmd FileType javascript inoreabbrev <buffer> pr Promise
	autocmd FileType javascript inoreabbrev <buffer> th this
	autocmd FileType javascript inoreabbrev <buffer> fa false
	autocmd FileType javascript inoreabbrev <buffer> tr true
	autocmd FileType javascript inoreabbrev <buffer> .t .then
	autocmd FileType javascript inoreabbrev <buffer> .c .catch
	autocmd FileType javascript inoreabbrev <buffer> ca catch
	autocmd FileType javascript inoreabbrev <buffer> idx index
	autocmd FileType javascript inoreabbrev <buffer> jtr JSON.stringify
	autocmd FileType javascript inoreabbrev <buffer> jpr JSON.parse
	autocmd FileType javascript inoreabbrev <buffer> tms timestamp
	autocmd FileType javascript inoreabbrev <buffer> @p @param
	autocmd FileType javascript inoreabbrev <buffer> @r @returns
	autocmd FileType javascript inoreabbrev <buffer> rrn req, res, next
	autocmd FileType javascript inoreabbrev <buffer> i18 i18n.t(
	autocmd BufEnter *.test.js setlocal nowrap
	autocmd BufEnter *.js highlight jsModel cterm=italic
	autocmd BufEnter *.js match jsModel /\<\w*Model\>/
	autocmd BufEnter *.js highlight jsErrorVariable ctermfg=red
	autocmd BufEnter *.js match jsErrorVariable /\%(\%(console\|logger\)\.\)\@10<!\<err\%[\%(or\)]\>/
	autocmd FileType javascript,json nnoremap <buffer> <Leader>, :normal! mmA,<C-[>`m
	autocmd FileType javascript nnoremap <buffer> <Leader>tt :tabe term://npm run test<CR>
	autocmd FileType javascript,c,cpp,rust setlocal signcolumn=yes
	autocmd FileType rust inoreabbrev <buffer> pln println!
augroup END
"»
" C Files«
augroup cfiles
	autocmd!
	autocmd BufRead,BufNewFile *.h setlocal filetype=c
	autocmd FileType c setlocal iskeyword+=#
	" Insert a comment block respecting the 42 norm
	autocmd FileType c nnoremap <buffer> <Leader>b o/*<CR><BS><BS><BS>**<CR>*/<CR><Esc>kkA
	autocmd FileType c noremap <Leader>H mm:call Create42Header()<CR>`m
	" (/Un)Comment a visual block
	autocmd FileType c vnoremap <buffer> <Leader>c <Esc>`>a */<Esc>`<i/* <Esc>
	autocmd FileType c vnoremap <buffer> <Leader>C <Esc>`>hhxxx`<xxx
	" These mappings are really cool except for 'for' loops, i should add
	" some vimscript to it
	" autocmd FileType c,cpp inoremap <buffer> ; <C-]>;<CR>
	" autocmd FileType c,cpp imap <buffer> <Esc>; <Esc><Leader>;
	autocmd FileType c,cpp inoremap <buffer> { {<CR>
	autocmd FileType c,cpp inoremap <buffer> } }<CR>
	" Highlight custom types
	autocmd BufEnter * syntax match cType "\<[t]_\w\+\>"
	autocmd BufEnter * syntax match cType "\<\w\+_[t]\>"
	autocmd FileType c,cpp,javascript inoreabbrev <buffer> ee else
	autocmd FileType c,cpp,javascript inoreabbrev <buffer> eef else if
	autocmd FileType c,cpp,javascript inoreabbrev <buffer> wh while
	autocmd FileType c,cpp inoreabbrev <buffer> vd void
	autocmd FileType c,cpp,javascript inoreabbrev <buffer> rn return
	autocmd FileType c,cpp,javascript inoreabbrev <buffer> ctn continue
	autocmd FileType c,cpp,javascript inoreabbrev <buffer> brk break
	autocmd FileType c,cpp,javascript inoreabbrev <buffer> dt data
	autocmd FileType c,cpp,javascript inoreabbrev <buffer> opt options
	autocmd FileType c,cpp inoreabbrev <buffer> mn main
	autocmd FileType c,cpp inoreabbrev <buffer> cn const
	autocmd FileType cpp inoreabbrev <buffer> uns using namespace
	autocmd FileType cpp inoreabbrev <buffer> co cout
	autocmd FileType cpp inoreabbrev <buffer> el endl
	autocmd FileType c inoreabbrev <buffer> null NULL
	autocmd FileType c inoreabbrev <buffer> nil '\0'
	autocmd FileType c inoreabbrev <buffer> r0 return (0)
	autocmd FileType c inoreabbrev <buffer> r1 return (1)
	autocmd FileType c,cpp inoreabbrev <buffer> #i #include 
	autocmd FileType c,cpp inoreabbrev <buffer> #d # define
	autocmd FileType c inoreabbrev <buffer> st size_t
	autocmd FileType c,cpp inoreabbrev <buffer> ch char
	autocmd FileType c inoreabbrev <buffer> uchar unsigned char
	autocmd FileType c inoreabbrev <buffer> uint unsigned int
	autocmd FileType c inoreabbrev <buffer> lint long int
	autocmd FileType c inoreabbrev <buffer> ulint unsigned long int
	autocmd FileType c inoreabbrev <buffer> fpf ft_printf(
	" ) self made printf used at 42
	autocmd FileType c inoreabbrev <buffer> pf printf(
	" )
augroup END
"»
" File specific autocommands«
augroup filespecific
	autocmd!
	autocmd BufRead .vimrc setlocal foldmethod=expr
	autocmd BufRead .vimrc setlocal foldexpr=getline(v:lnum)[0]==\"\\t\"
	autocmd BufRead .vimrc setlocal nowrap
	autocmd BufRead,BufNewFile .spacemacs setlocal filetype=lisp
	autocmd BufRead,BufNewFile .eslintrc setlocal filetype=json
augroup END
"»
" Function definition«
function! RemoveExtraWhiteSpaces()
	if !&binary && &filetype != 'diff'
		" preparation: save last search, and cursor position.
		let _s=@/
		let l = line(".")
		let c = col(".")
		echom "cleaning"
		" Whitespaces at end of line
		%s/\s\+$//e
		"  Extra blank lines
		%s/\n\{2}\zs\n\+//e
		" Blank lines at end of file
		%s/\($\n\)\+\%$//e
		" Empty lines before closing braces
		%s/\%(^\s*$\n\)\+\ze\s*}//ge
		" Whitespaces before comma     
		%s/\s\+\ze,//ge
		" Too many whitespaces after comma or no whitespaces
		%s/,\zs\%(\s\{2,}\|\ze[^ ]\)/ /ge
		" Parenthesis not sticking
		%s/\s\+\ze)//ge
		%s/(\zs\s\+//ge
		if &filetype == 'c'
			" Function bodies not separated by a blank line
			0,$-1s/^}\n\n\@!/}\r\r/e
		endif
		" clean up: restore previous search history, and cursor position
		let @/=_s
		call cursor(l, c)
	endif
endfunction
"»
" Stop being a noob :P«
" Who needs the arrow keys ?
" noremap <Up> <Nop>
" noremap <Down> <Nop>
" noremap <Left> <Nop>
" noremap <Right> <Nop>
" inoremap <Up> <Nop>
" inoremap <Down> <Nop>
" inoremap <Left> <Nop>
" inoremap <Right> <Nop>
" noremap <PageUp> <Nop>
" noremap <PageDown> <Nop>
" inoremap <PageUp> <Nop>
" inoremap <PageDown> <Nop>
" This is weird but i always get confused when switching between
" ANSI and ISO keyboards and find myself typing backslashes
" Instead i use <C-J>
" Unfortunately this mapping also disables <C-M> since they both have
" the same code
" noremap <CR> <Nop>
" inoremap <CR> <Nop>
" cnoremap <CR> <Nop>
" I got so used to this that i don't use <CR> anymore, however i need the
" <C-M> mapping to work since <C-J> isn't so easy to type on dvorak...
" I find <C-H> to be a lot more confortable than <BS>
" It is hilarious to see how others are confused when they realize
" backspace doesn't work
" noremap <BS> <Nop>
" inoremap <BS> <Nop>
" cnoremap <BS> <Nop>
" My right pinky feels a lot better after these mappings
"»
" 42Header«
" Not my function, i copy/pasted it from somewhere on Github
let g:mail42="florenzo@student.42.fr"
let g:username42="florenzo"
"create a header if we need it
function! Create42Header()
	let is_header = Create42Header_if_exist()
	if is_header == 0
		call Create42Header_create()
	else
		echo 'you already have a header'
	endif
endfunction
" check if the header exist
function! Create42Header_if_exist()
	let begin = '# '
	let end = ' #'
	let size_c = 1
	if expand('%:e') == 'c' || expand('%:e') == 'h' || expand('%:e') == 'cpp'
		let begin = '\/\*'
		let end = '\*\/'
		let size_c = 2
	elseif expand('%:e') == 'vim' || expand('%:t') == '.vimrc'
		let begin = '" '
		let end = ' "'
		let size_c = 1
	endif
	:normal gg
	let is_header =  search(l:begin . '\s*\*\{' . (78 - 2 * l:size_c) . '}\s*'
				\. l:end . '\n
				\' . l:begin . '\s\+' . l:end . '\n
				\' . l:begin . '\s\+:::\s\+:\{8}\s\+' . l:end . '\n
				\' . l:begin . '\s\+\w\+.\w* \+:+:\s\+:+:\s\+:+:\s\+' . l:end
				\. '\n
				\' . l:begin . '\s\++:+ +:+\s\++:+\s\+' . l:end . '\n
				\' . l:begin . '\s\+By: \w\+ <.\+> \++#+  +:+ \{7}+#+ \{8}' .
				\l:end . '\n
				\' . l:begin . '\s\++#+#+#+#+#+\s\++#+\s\+' . l:end . '\n
				\' . l:begin . '\s\+Created: 20\d\d\/\d\d\/\d\d \d\d:\d\d:\d\d
				\ by \w\+\s\+#+#\s\+#+#\s\+' . l:end . '\n
				\' . l:begin . '\s\+Updated: 20\d\d\/\d\d\/\d\d \d\d:\d\d:\d\d
				\ by \w\+\s\+###\s\+#\{8}\.fr\s\+' . l:end . '\n
				\' . l:begin . '\s\+' . l:end . '\n
				\' . l:begin . '\s*\*\{' . (78 - 2 * l:size_c) . '}\s*'
				\. l:end . '\n', 'c', line('0'))
	if line('.') > 1
		let linedown = line('.') - 1
		if search('^' . repeat('\n', linedown) . '\/\* *', 'c', line('0')) == 1
			:normal gg
			exe ':normal! d'. linedown . 'd'
		else
			echo 'header not a the top of the file'
		endif
	endif
	return is_header
endfunction
" create a header
function! Create42Header_create()
	let begin = '# '
	let end = ' #'
	let size_c = 1
	if expand('%:e') == 'c' || expand('%:e') == 'h' || expand('%:e') == 'cpp'
		let begin = '\/\*'
		let end = '\*\/'
		let size_c = 2
	elseif expand('%:e') == 'vim' || expand('%:t') == '.vimrc'
		let begin = '" '
		let end = ' "'
		let size_c = 1
	elseif expand('%:t') == '.emacs'
		let begin = '; '
		let end = ' ;'
		let size_c = 1
	endif
	"expand for srcs/main.c
	"	%:t main.c
	"	%:e c
	"	%:r srcs/main.c
	"	%:r:t srcs/main
	let filename = expand('%:t')
	" let time_act = strftime('%Y\/%m\/%d %H:%M:%S')
	let time_act = '4242\/42\/42 42:42:42'
	echo strlen(time_act)
	let user = g:username42
	let mail = g:mail42
	:normal gg
	exe ':1 s/^/' . l:begin . repeat(' ', size_c - 1) .
				\repeat('*', 78 - 2 * size_c) . repeat(' ', size_c - 1) .
				\l:end . '\r'
	exe ':2 s/^/' . l:begin . repeat(' ', 76) . l:end . '\r'
	let line42 = ':::' . repeat(' ', 6) . '::::::::' . repeat(' ', 3)
	exe ':3 s/^/' . l:begin . repeat(' ', 56) . line42 . l:end . '\r'
	let line42 = ':+:' . repeat(' ', 6) . ':+:' . repeat(' ', 4) . ':+:' .
				\repeat(' ', 3)
	exe ':4 s/^/' . l:begin . repeat(' ', 3) . filename .
				\repeat(' ', 51 - strlen(filename)) .  line42 . l:end . '\r'
	let line42 = '+:+ +:+' . repeat(' ', 9) . '+:+' . repeat(' ', 5)
	exe ':5 s/^/' . l:begin . repeat(' ', 52) . line42 . l:end . '\r'
	let line42 = '+#+  +:+' . repeat(' ', 7) . '+#+' . repeat(' ', 8)
	exe ':6 s/^/' . l:begin . repeat(' ', 3) . 'By: ' . user . ' <' . mail . '>'
				\. repeat(' ', 40 - strlen(user) - strlen(mail)) .
				\line42 . l:end . '\r'
	let line42 = '+#+#+#+#+#+' . repeat(' ', 3) . '+#+' . repeat(' ', 11)
	exe ':7 s/^/' . l:begin . repeat(' ', 48) . line42 . l:end . '\r'
	let line42 = '#+#' . repeat(' ', 4) . '#+#' . repeat(' ', 13)
	exe ':8 s/^/' . l:begin . repeat(' ', 3) . 'Created: ' . time_act . ' by '
				\. user . repeat(' ', 39 - strlen(time_act) - strlen(user)) .
				\line42 . l:end . '\r'
	let line42 = '###' . repeat(' ', 3) . '########.fr' . repeat(' ', 7)
	exe ':9 s/^/' . l:begin . repeat(' ', 3) . 'Updated: ' . time_act . ' by '
				\. user .  repeat(' ', 38 - strlen(time_act) - strlen(user)) .
				\line42 . l:end . '\r'
	exe ':10 s/^/' . l:begin . repeat(' ', 76) . l:end . '\r'
	exe ':11 s/^/' . l:begin . repeat(' ', size_c - 1) .
				\repeat('*', 78 - 2 * size_c) . repeat(' ', size_c - 1) .
				\l:end . '\r\r'
endfunction
"»
" vim: set foldmethod=marker foldmarker=«,»:
